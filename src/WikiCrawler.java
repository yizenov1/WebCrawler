import java.io.*;
import java.net.*;
import java.util.*;

public class WikiCrawler
{
    //TODO: is this global?
    //TODO: why is it static, constant versus final?
    public static final String BASE_URL = "https://en.wikipedia.org";
    
    private final String robotFile = "robots.txt"; //this might cause an error
    private final Set<String> disAllowedLinks;
    private char[] chars = {'#', ':'};
    
    private String seedUrl, fileName;
    private int max, counter;
    private boolean isWeighted;
    private String[] keywords;
    
    private WeightedQ weightedQueue;
    //private Queue<String> regularQueue;
    
    private Hashtable<String, HashSet<String>> vertices;
    private HashSet<String> distinctVertices;
        
    WikiCrawler(String seedUrl, String[] keywords, int max, String fileName, boolean isWeighted) {
        this.seedUrl = seedUrl;
        this.keywords = keywords;
        this.max = max;
        this.fileName = fileName;
        this.isWeighted = isWeighted;
        counter = 0;
        
        weightedQueue = new WeightedQ();
        //regularQueue = new PriorityQueue<String>();
        
        distinctVertices = new HashSet<String>();
        vertices = new Hashtable<String, HashSet<String>>();
        disAllowedLinks = new HashSet<String>();
    }
    
    void crawl() {
        ReadDocument(robotFile);
        WeightedBFS();
        WriteIntoFile(fileName);
    }
    
    private void WeightedBFS() {

        int tenRequestCounter = 0;
        
        //I don't check the links for existence
        if (!IsItInRobotsTxt(seedUrl) || !CheckForImagesAndSections(seedUrl)) {
            distinctVertices.add(seedUrl);
            counter++;
        }
        
        if (isWeighted) weightedQueue.add(seedUrl, 1.0);
        else weightedQueue.add(seedUrl, 0.0);
        //else regularQueue.add(seedUrl);
        
        String url;
 
        //while (counter < max) {
        while (weightedQueue.size() != 0) {
            
            url = weightedQueue.extract();
            //if (isWeighted) url = weightedQueue.extract();
            //else url = regularQueue.poll();

            String page_content = SendRequest(BASE_URL + url);

            if (!vertices.contains(url)) {
                HashSet<String> outlinks = new HashSet<String>();
                vertices.put(url, outlinks);
            }
            
            ExtractUrls(page_content, url);
            
            tenRequestCounter++;
            if (tenRequestCounter == 10) {
                tenRequestCounter = 0;
                try {
                    Thread.sleep(1000); // 1 seconds
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }
    
    private String SendRequest(String url) {
        
        URL root;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        String page_content = "";

        try {
            
            root = new URL(url);
            inputStream = root.openStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            
            boolean found = false;
            
            String line = bufferedReader.readLine();
            while(!line.isEmpty() || line.equals("")) {
                
                //TODO: is this enough and when to stop?
                if (line.contains("<p>") || found) {         
                    if (!found) found = true;
                    page_content += line;
                }
                line = bufferedReader.readLine();
                if (line == null) break;
            }
            
        } catch (Exception e) {
            //TODO: am I catching url exceptions?
            e.printStackTrace();
        } finally {
            
            if(bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return page_content;
    }
    
    private void ExtractUrls(String text, String url) {

        HtmlParser htmlParser = new HtmlParser();
        List<HtmlLink> urls = htmlParser.extractLinks(text);
        
        String[] words = new String[0], hyperText;
        String hyperLink, plain_text = "";
        int pointer = 0, index = 0;
        double minBefore = 0.0, minAfter = 0.0;
        boolean found;
        
        for (HtmlLink extracted_url: urls) {
            
            if (IsItInRobotsTxt(extracted_url.getLink())) continue;
            if (CheckForImagesAndSections(extracted_url.getLink())) continue;
            
            hyperLink = extracted_url.getLink();
            
            // in order to avoid self loops and multiple edges
            if (hyperLink.equals(url)) continue;
            Set<String> set = vertices.get(url);
            if (!set.contains(hyperLink) ) {
                set.add(hyperLink);  
            }
            
            if (counter < max) {
                if (!distinctVertices.contains(hyperLink)) counter++;
            } else {
                break;
            }
            
            if (isWeighted) {
                found = false;
                
                for (String keyword: keywords) {
                    if (hyperLink.contains(keyword) || hyperLink.contains(keyword)) {
                        weightedQueue.add(hyperLink, 1.0);
                        found = true;
                        break;
                    }
                }
            
                if (!found) {
                    if (plain_text.isEmpty()) {
                        plain_text = htmlParser.extractText(text);
                        words = plain_text.split(" ");
                    }
                    
                    hyperText = extracted_url.getLinkText().split(" ");
        
                    for (int i = pointer; i < words.length; i++) {
    
                        if (words[i].equals(hyperText[0])) {
                            index = i + 1;
                            for (int j = 1; j < hyperText.length; j++) {
                                if (!words[index].equals(hyperText[j])) {
                                    found = true;
                                    break;
                                }
                                index++;
                            }
                        }
                        
                        if (found) {
                            
                            found = false;
                            //20 words before the hyper link
                            if (i <= 20) {
                                index = i - 1;     
                                while (index > -1) {
                                    for (String keyword: keywords) {
                                        if (words[index].equals(keyword)) {
                                            found = true;
                                            minBefore = (double) 1 / (i - index + 2);
                                            break;
                                        }
                                    }
                                    if (found) break;
                                    index--;
                                }
                            } else {
                                index = i - 1;
                                while (i - index <= 20) {
                                    for (String keyword: keywords) {
                                        if (words[index].equals(keyword)) {
                                            found = true;
                                            minBefore = (double) 1 / (i - index + 2);
                                            break;
                                        }
                                    }
                                    if (found) break;
                                    index--;
                                }
                                if (!found) minBefore = 0.0; //TODO: zero or infinity?
                            }
                            
                            pointer = i + hyperText.length;
                            i = pointer;
                            
                            found = false;
                            //20 words after the hyper link
                            if (i + 20 <= words.length) {
                                index = i + 1;
                                while (index < words.length) {
                                    for (String keyword: keywords) {
                                        if (words[index].equals(keyword)) {
                                            found = true;
                                            minAfter = (double) 1 / (index - i + 2);
                                            break;
                                        }
                                    }
                                    if (found) break;
                                    index++;
                                }
                            } else {
                                index = i + 1;
                                while (index < i + 20 && index < words.length) { //there is an error
                                    for (String keyword: keywords) {
                                        if (words[index].equals(keyword)) {
                                            found = true;
                                            minAfter = (double) 1 / (index - i + 2);
                                            break;
                                        }
                                    }
                                    if (found) break;
                                    index++;
                                }
                                if (!found) minAfter = 0.0; //TODO: zero or infinity?         
                            }
                            
                            if (minBefore <= minAfter) {
                                weightedQueue.add(hyperLink, minBefore);
                            } else {
                                weightedQueue.add(hyperLink, minAfter);
                            }
                            break;
                        }
                    }
                }
            } else {
                weightedQueue.add(hyperLink, 0.0);
                //regularQueue.add(hyperLink);         
           }
        }
    }
    
    private boolean CheckForImagesAndSections(String url) {
        for (int i = 0; i < url.length(); i++) {
            if (url.charAt(i) == chars[0] || url.charAt(i) == chars[1]) return true;
        }
        
        return false;
    }
    
    private boolean IsItInRobotsTxt(String url) {
        if (disAllowedLinks.contains(url)) return true;
        return false;
    }
    
    private void WriteIntoFile(String filePath) {
        
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        
        try {
            
            File file = new File(filePath);
            if (!file.exists()) file.createNewFile();
            else {
                file.delete();
                file.createNewFile();
            }
                
            fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
            
//            for (String key: keys) {
//            	values = vertices.get(key);
//            	if (!distinctVertices.contains(key)) distinctVertices.add(key);
//            	for (String value: values) {
//    		if (!distinctVertices.contains(value)) distinctVertices.add(value);
//            	}
//            }
            fileWriter.write(distinctVertices.size() + "\n");
            
            Set<String> keys = vertices.keySet();
            Set<String> values;
            for (String key: keys) {
                values = vertices.get(key);
                for (String value: values) {
                    fileWriter.write(key + " " + value + "\n");
                }
            }
            
        } catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + filePath + "'");
        } catch (Exception e) {
            System.out.println("Error reading file '" + filePath + "'");
        } finally {        
            if(bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
            if(fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    private void ReadDocument(String filePath) {
        
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
       
        String line;
        String[] words;
        
        try {
            fileReader = new FileReader(filePath);
            bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                if (line.contains("Disallow:")) {
                    words = line.split(" ");
                    if (words.length > 1) disAllowedLinks.add(words[1]);
                }
            }
        } catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + filePath + "'");
        } catch (Exception e) {
            System.out.println("Error reading file '" + filePath + "'");
        } finally {        
            if(bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
            if(fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
