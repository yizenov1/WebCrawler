import java.util.*;

public class WeightedQ
{   
    private PriorityQueue<Obj> queue;

    public WeightedQ() {
        queue = new PriorityQueue<Obj>(new ObjComparator());
    }
    
    public void add(String x, double weight) {  
        if(!queue.contains(new Obj(x, weight))) {
            queue.add(new Obj(x, weight));
        }
    }
    
    public String extract() {
        Obj obj = queue.poll();
        return obj.getObj();
    }
    
    public int size() {
        return queue.size();
    }
    
    public class ObjComparator implements Comparator<Obj> {
        
        @Override
        public int compare(Obj obj1, Obj obj2) {    
            if(obj1.getWeight() < obj2.getWeight()) return 1;
            if(obj1.getWeight() > obj2.getWeight()) return -1;
            
            Obj newer = Obj.newOne(obj1, obj2);
            
            if(newer == obj1) return 1;
            else if(newer == obj2) return -1;
            return 0;
        }
    }
}