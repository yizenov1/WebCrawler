
public class MyWikiRanker
{
    private String[] topicWords;
    private String seedUrl;
    private int numberOfVertices;
    
    MyWikiRanker() {
        this.topicWords = new String[] {"chess", "board game", "player", "check", "mate", "debut"};   
        this.seedUrl = "/wiki/Chess";
        this.numberOfVertices = 1000;
    }
    
    public String[] getTopicWords() {
        return topicWords;
    }
    
    public String getSeedUrl() {
        return seedUrl;
    }
    
    public int getNumberOfVertices() {
        return numberOfVertices;
    }
}
