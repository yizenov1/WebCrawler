import java.util.*;

public class WikiTennisRanker
{
    public static void main(String[] args) {
        // TODO Auto-generated method stub
    	
    	//TestWeightedQ();
    	//TestUnWeightedQ();

//        String[] topicWords = new String[] {"tennis", "grand", "slam", "french open", "australian open", 
//                "wimbledon", "US open", "masters"};
//        String fileName = "wikiTennis.txt";    
//        Run("/wiki/Tennis", topicWords, 1000, fileName, true);  

        String myFileName = "wikiChess.txt";
        MyWikiRanker myRanker = new MyWikiRanker();
        
        WikiCrawler wikiCrawler = new WikiCrawler(myRanker.getSeedUrl(), myRanker.getTopicWords(), myRanker.getNumberOfVertices(), myFileName, true);
        wikiCrawler.crawl();
        
        Run(myFileName);
    }
    
    private static void Run(String fileName) {
        
        double[] epsilons = {0.01, 0.005};    
        PageRank pageRank;
        List<String[]> list = new LinkedList<String[]>();
        
        for (double epsilon: epsilons) {
            pageRank = new PageRank(fileName, epsilon);
            list.clear();
            
            String[] top10PageRank = pageRank.topKPageRank(10);
            list.add(top10PageRank);
            String[] top10InDegree = pageRank.topKInDegree(10);
            list.add(top10InDegree);
            String[] top10OutDegree = pageRank.topKOutDegree(10);
            list.add(top10OutDegree);
            
            //TODO: do I need to output to console without any description?
            for (int i = 0; i < list.size(); i++) {
                String[] elements = list.get(i);
                for (int j = 0; j < elements.length; j++) {
                    System.out.println(elements[j]);
                }
                System.out.println();
            }
            
            String[] top100PageRank = pageRank.topKPageRank(100);
            String[] top100InDegree = pageRank.topKInDegree(100);
            String[] top100OutDegree = pageRank.topKOutDegree(100);
            
            Set A = new HashSet(Arrays.asList(top100InDegree));
            Set B = new HashSet(Arrays.asList(top100OutDegree));
            Set C = new HashSet(Arrays.asList(top100PageRank));
            
            System.out.println("PageRank vs InDegree: " + CalculateExactJaccardSim(A, B));
            System.out.println("PageRank vs OutDegree: " + CalculateExactJaccardSim(A, C));
            System.out.println("InDegree vs OutDegree: " + CalculateExactJaccardSim(B, C));
            System.out.println();
        }
    }
    
    private static double CalculateExactJaccardSim(Set a, Set b) {
        Set<?> interSection = new HashSet(a);
        Set<?> union = new HashSet(a);
        interSection.retainAll(b);
        union.addAll(b);
        return Double.valueOf(interSection.size()) / Double.valueOf(union.size());
    }
     
    private static void TestWeightedQ() {
        WeightedQ weightedQ = new WeightedQ();

        weightedQ.add("1", 5);
        weightedQ.add("2", 3);
        weightedQ.add("5", 7);
        weightedQ.add("21", 5);
        weightedQ.add("36", 4);
        
        System.out.println(weightedQ.extract());
        System.out.println(weightedQ.extract());
        
        weightedQ.add("21", 9);
        
        while (weightedQ.size() != 0) {
            System.out.println(weightedQ.extract());
        }
    }
    
    private static void TestUnWeightedQ() {
        WeightedQ weightedQ = new WeightedQ();
        weightedQ.add("1", 0);
        weightedQ.add("2", 0);
        weightedQ.add("36", 0);
        weightedQ.add("21", 0);
        weightedQ.add("13", 0);
        
        System.out.println(weightedQ.extract());
        System.out.println(weightedQ.extract());
        
        weightedQ.add("21", 0);
        
        while (weightedQ.size() != 0) {
            System.out.println(weightedQ.extract());
        }
    }
}
