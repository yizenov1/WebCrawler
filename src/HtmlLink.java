
public class HtmlLink
{
    private String link;
    private String linkText;

    HtmlLink(){ };

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = replaceInvalidChar(link);
    }

    public String getLinkText() {
        return linkText;
    }

    public void setLinkText(String linkText) {
        this.linkText = linkText;
    }

    private String replaceInvalidChar(String link){
        link = link.replaceAll("'", "");
        link = link.replaceAll("\"", "");
        return link;
    }
}
