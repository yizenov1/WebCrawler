
public class Obj {

        private String obj;
        private double weight;
        private long timeAddedToQueue;

        public Obj(String obj, double weight) {
            this.obj = obj;
            this.weight = weight;
            setTimeAddedToQueue();
        }
        
        public String getObj() {
            return obj;
        }

        public double getWeight() {
            return weight;
        }

        public void setWeight(double weight) {
            this.weight = weight;
        }
        
        public long getTimeAddedToQueue() {
            return timeAddedToQueue;
        }

        public void setTimeAddedToQueue() {
            this.timeAddedToQueue = System.nanoTime();
        }

        public static Obj newOne(Obj obj1, Obj obj2) {
            return (obj1.getTimeAddedToQueue() > obj2.getTimeAddedToQueue()) ? obj1 : obj2;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj.getClass() != Obj.class) return false;
            Obj other = (Obj)obj;
            if (this.getObj().equals(other.getObj())) return true;
            else return false;
        }
    }
