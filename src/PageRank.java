import java.io.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;

public class PageRank
{
    private double epsilon;
    private double beta = 0.85;
    
    private double[][] matrix;
    private double[] rank;
    private Map<String, int[]> vertexIndices;
    private String[] index_vertexName;
    
    private int numberOfVertices, numberOfEdges;
    
    List<PageWithRank> sortedPagesByRank;
    List<PageWithInDegree> sortedPagesByInDegree;
    List<PageWithOutDegree> sortedPagesByOutDegree;
    
    PageRank(String fileName, double epsilon) {
        this.epsilon = epsilon;
        
        //filePath - this can cause null point exception because of the location
        ReadDocument(fileName);
        
        //testInAndOutDegree();
        CalculateInAndOutDegrees();
        
        ComputePageRank();
        
        sortedPagesByRank = new LinkedList<PageWithRank>();
        sortedPagesByInDegree = new LinkedList<PageWithInDegree>();
        sortedPagesByOutDegree = new LinkedList<PageWithOutDegree>();
        
        Sort();
    }
    
    private void ReadDocument(String filePath) {
        
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
       
        String line;
        int i, j;
        
        try {
            fileReader = new FileReader(filePath);
            bufferedReader = new BufferedReader(fileReader);
            
            if ((line = bufferedReader.readLine()) != null) {
                
                numberOfVertices = Integer.valueOf(line);
                numberOfEdges = 0;
                matrix = new double[numberOfVertices][numberOfVertices];
                rank = new double[numberOfVertices];
                index_vertexName = new String[numberOfVertices];
                
                int index = 0;
                String[] vertices;
                int[] tuple;
                vertexIndices = new HashMap<String, int[]>();
            
                while((line = bufferedReader.readLine()) != null) {
                    //TODO: can this be in main memory?
       
                    vertices = line.split(" ");
                    for (String vertex: vertices) {
                        if (!vertexIndices.containsKey(vertex)) {
                            tuple = new int[3];
                            tuple[0] = index;
                            index_vertexName[index] = vertex;
                            vertexIndices.put(vertex, tuple);
                            index++;
                        }
                    }
                    
                    i = vertexIndices.get(vertices[0])[0];
                    j = vertexIndices.get(vertices[1])[0];
                    
                    // here we can avoid duplicates
                    if (matrix[j][i] != 1.0) {
                        matrix[j][i] = 1.0; //////////////////////////////////////// HERE MIGHT BE A MISTAKE
                        numberOfEdges++;
                    }
                }
            }

            //TODO: do I have to close them every time?
            bufferedReader.close();
            fileReader.close();
                
        } catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + filePath + "'");
        } catch (Exception e) {
            System.out.println("Error reading file '" + filePath + "'");
        } finally {        
            if(bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
            if(fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    private void CalculateInAndOutDegrees() {
        int in_degree = 0, out_degree = 0;
        
        for (int i = 0; i < numberOfVertices; i++) {
            for (int j = 0; j < numberOfVertices; j++) {
                
                if (matrix[i][j] == 1.0) in_degree++;
                else matrix[i][j] = (double) 1 / numberOfVertices; // replacing all zeros
                
                if (matrix[j][i] == 1.0) out_degree++;
            }
            
            vertexIndices.get(index_vertexName[i])[1] = in_degree;
            vertexIndices.get(index_vertexName[i])[2] = out_degree;
            
            in_degree = 0;
            out_degree = 0;
        }
    }
    
    private void testInAndOutDegree() {
        numberOfVertices = 4;
        matrix = new double[][]{{0,0,3,0}, {2,0,0,1}, {1,2,0,0}, {1,0,0,0}};
    }
    
    private void ComputePageRank() {
        
        int counter = 0; //we don't need this so far
        boolean converged = false;
        
        rank = new double[numberOfVertices];
        
        for (int i = 0; i < rank.length; i++) {
            rank[i] = (double) 1 / numberOfVertices;
        }
        
        double[] old_rank;
        double val;
        while (!converged) {
            old_rank = rank;
            rank = CalculateNextRank(rank);
            
            val = CalculateNorm(rank, old_rank);
            if (val <= epsilon)
                converged = true; //TODO: which one to return?
            counter++;
        }
        
        //System.out.println(counter);
    }
    
    private double[] CalculateNextRank(double[] currentRank) {
        
        double[] newRank = new double[numberOfVertices];
        
        double default_value = (double) ((1.0 - beta) / numberOfVertices);
        
        for (int i = 0; i < newRank.length; i++) {
            newRank[i] = default_value;
        }
        
//        double val;
//        for (int[] tuple: vertexIndices.values()) {
//            if (tuple[2] != 0) {
//                val = default_value + beta * (double) (currentRank[tuple[0]] / tuple[2]);
//                newRank[tuple[0]] = val;
//                ////////////////////////////////////////HERE MIGHT BE A MISTAKE
//            } else {
//                val = default_value + beta * (double) (currentRank[tuple[0]] / numberOfVertices);
//                newRank[tuple[0]] = val;
//                ////////////////////////////////////////HERE MIGHT BE A MISTAKE
//            }
//        }
        
        double val;
        int lenght;
        
        for (int[] tuple: vertexIndices.values()) {
            if (tuple[2] != 0) { 
                lenght = matrix.length;
                val = beta * (double) (currentRank[tuple[0]] / tuple[2]);
                for (int i = 0; i < lenght; i++) {
                    if (matrix[i][tuple[0]] == 1.0) {
                        newRank[i] += val;                      
                    }
                }
            } else {
                val = beta * (double) (currentRank[tuple[0]] / numberOfVertices);
                for (int i = 0; i < newRank.length; i++) {        
                    newRank[i] += val;
                }
            }
        }
        return newRank;
    }
    
    
    private double CalculateNorm(double[] previous_rank, double[] current_rank) {
        double norm = 0.0;
        
        for (int i = 0; i < numberOfVertices; i++) {
            norm += Math.abs(previous_rank[i] - current_rank[i]);
        }
        
        return norm;
    }
    
    
    private void Sort() {
        //TODO: using a lot memory
        
        PageWithRank pageWithRank;
        PageWithInDegree pageWithInDegree;
        PageWithOutDegree pageWithOutDegree;
        
        String page;
        int[] tuple;
        
        for (int i = 0; i < rank.length; i++) {
            page = index_vertexName[i];
            pageWithRank= new PageWithRank(page, rank[i]);
            sortedPagesByRank.add(pageWithRank);
            
            tuple = vertexIndices.get(page);
            pageWithInDegree = new PageWithInDegree(page, tuple[1]);
            sortedPagesByInDegree.add(pageWithInDegree);
            pageWithOutDegree = new PageWithOutDegree(page, tuple[2]);
            sortedPagesByOutDegree.add(pageWithOutDegree);
        }
        
        Collections.sort(sortedPagesByRank);
        Collections.reverse(sortedPagesByRank);
        
        Collections.sort(sortedPagesByInDegree);
        Collections.reverse(sortedPagesByInDegree);
        
        Collections.sort(sortedPagesByOutDegree);
        Collections.reverse(sortedPagesByOutDegree);
    }
    
    
    class PageWithRank implements Comparable<PageWithRank> {
        private final String vertex;
        private final Double rank;
        
        public PageWithRank(String vertex, double rank) {
            this.vertex = vertex;
            this.rank = rank;
        }
        
        public String getVertex() { return vertex; }
        public double getRank() { return rank; }
        
        public int compareTo(PageWithRank obj) {
            return rank.compareTo(obj.rank);
        }
    }
    
    
    class PageWithInDegree implements Comparable<PageWithInDegree> {
        private final String vertex;
        private final Integer in_degree;
        
        public PageWithInDegree(String vertex, int in_degree) {
            this.vertex = vertex;
            this.in_degree = in_degree;
        }
        
        public String getVertex() { return vertex; }
        public int getIndegree() { return in_degree; }
        
        public int compareTo(PageWithInDegree obj) {
            return in_degree.compareTo(obj.in_degree);
        }
    }
    
    
    class PageWithOutDegree implements Comparable<PageWithOutDegree> {
        private final String vertex;
        private final Integer out_degree;
        
        public PageWithOutDegree(String vertex, int out_degree) {
            this.vertex = vertex;
            this.out_degree = out_degree;
        }
        
        public String getVertex() { return vertex; }
        public int getOutDegree() { return out_degree; }

        public int compareTo(PageWithOutDegree obj) {
            return out_degree.compareTo(obj.out_degree);
        }
    }

    double pageRankOf(String vertex) {
        int index = vertexIndices.get(vertex)[0];
        return rank[index];
    }
    
    int outDegreeOf(String vertex) {
        return vertexIndices.get(vertex)[2];
    }
    
    int inDegreeOf(String vertex) {
        return vertexIndices.get(vertex)[1];
    }
    
    int numEdges() {
        return numberOfEdges;
    }
    
    String[] topKPageRank(int k) {
        String[] pages = new String[k];
        
        for (int i = 0; i < k; i++) {
            PageWithRank page = sortedPagesByRank.get(i);
            pages[i] = page.getVertex();
        }
        
        return pages;
    }
    
    String[] topKInDegree(int k) {
        String[] pages = new String[k];
        
        for (int i = 0; i < k; i++) {
            PageWithInDegree page = sortedPagesByInDegree.get(i);
            pages[i] = page.getVertex();
        }
        
        return pages;
    }
    
    String[] topKOutDegree(int k) {
        String[] pages = new String[k];
        
        for (int i = 0; i < k; i++) {
            PageWithOutDegree page = sortedPagesByOutDegree.get(i);
            pages[i] = page.getVertex();
        }
        
        return pages;
    }
}
